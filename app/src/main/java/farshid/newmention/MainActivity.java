package farshid.newmention;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<String> strings;

    MentionableEditText mentionableEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        strings = new ArrayList<>();

        strings.add("farshid");
        strings.add("masoud");
        strings.add("farhad");
        strings.add("farangis");
        strings.add("farjad");
        strings.add("farhaneh");

        mentionableEditText = (MentionableEditText) findViewById(R.id.qwe);
        mentionableEditText.setInputList(strings);
    }
}