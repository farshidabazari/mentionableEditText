package view;

import java.util.ArrayList;

/**
 * @author FarshidAbz
 * @version 1.0
 * @since 9/9/2016
 */
public interface MainView {
    void setSuggestionList(ArrayList<String> suggestionList);
}
